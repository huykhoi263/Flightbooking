
/**
 * Module dependencies.
 */


var express = require('express'),

    http = require('http'),
    path = require('path'),

    config = require('./config')(),

    app = express(),

    MongoClient = require('mongodb').MongoClient,

    Admin = require('./controllers/Admin'),

    Home = require('./controllers/Home'),

    Tour = require('./controllers/Tour'),

    Price = require('./controllers/Price');


// all environments
// 
app.set('port', process.env.PORT || 3000);

app.set('views', __dirname + '/templates');

app.set('view engine', 'hjs');

app.use(express.favicon());

app.use(express.logger('dev'));

app.use(express.bodyParser());

app.use(express.methodOverride());

app.use(express.cookieParser('fast-delivery-site'));

app.use(express.session());

app.use(app.router);

app.use(require('less-middleware')({ src: __dirname + '/public' }));

app.use(express.static(path.join(__dirname, 'public')));


// development only
if ('development' == app.get('env')) 
{
  	
	app.use(express.errorHandler());

}


MongoClient.connect('mongodb://' + config.mongo.host + ':' + config.mongo.port + '/fastdelivery', function(err, db)
{

	if(err) 
	{

		console.log('Sorry, there is no mongo db server running.');

	} 
	else 
	{

		var attachDB = function(req, res, next) 
		{

			req.db = db;

			next();

		};

	app.all('/admin*', attachDB, function(req, res, next)
	{
	Admin.run(req, res, next);
		});
	app.all('/tour/:id', attachDB, function(req, res, next) 
	{	Tour.runArticle(req, res, next);	});
	
app.all('/tour', attachDB, function(req, res, next) 
	{	Tour.run(req, res, next);});
app.all('/price', attachDB, function(req, res, next) 
	{	Price.run('price', req, res, next);	});
app.all('/careers', attachDB, function(req, res, next) 
	{	Page.run('careers', req, res, next);	});
app.all('/contacts', attachDB, function(req, res, next) 
	{	Page.run('contacts', req, res, next);	});
	
app.all('/', attachDB, function(req, res, next) 
	{
	Home.run(req, res, next);
		});
	http.createServer(app).listen(config.port, function() 
	{	  
		console.log('Successfully connected to mongodb://' + config.mongo.host + ':' + config.mongo.port,'\nExpress server listening on port ' + config.port
);

	});

	}

});